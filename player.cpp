// Programming 2D Games
// Copyright (c) 2011,2013 by: 
// Charles Kelly
// player.cpp v1.0

#include "player.h"
#include "tileCollision.h"

//=============================================================================
// default constructor
//=============================================================================
Player::Player() : Entity()
{
    spriteData.width = playerNS::WIDTH;   // player size
    spriteData.height = playerNS::HEIGHT;
    spriteData.x = playerNS::X;           // location on screen
    spriteData.y = playerNS::Y;
    spriteData.rect.bottom = playerNS::HEIGHT;    // rectangle to select parts of an image
    spriteData.rect.right = playerNS::WIDTH;
    edge.top    = playerNS::EDGE_TOP;             // BOX collision edges
    edge.bottom = playerNS::EDGE_BOTTOM;
    edge.left   = playerNS::EDGE_LEFT;
    edge.right  = playerNS::EDGE_RIGHT;
    velocity.x = 0;
    velocity.y = 0;
    frameDelay = playerNS::ANIMATION_DELAY;
    startFrame = playerNS::START_FRAME;           // first frame of animation
    endFrame     = playerNS::END_FRAME;           // last frame of animation
    currentFrame = startFrame;
    radius = playerNS::WIDTH/2.0;
    collisionType = entityNS::ROTATED_BOX;
    mass = playerNS::MASS;
	health = 100;
	allowMovement = true;
}

//=============================================================================
// Initialize the player.
// Post: returns true if successful, false if failed
//=============================================================================
bool Player::initialize(Game *gamePtr, int width, int height, int ncols,
						TextureManager *textureM)
{
    return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}

void Player::loseHealth(int amount)
{
	health -= amount;
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void Player::update(float frameTime)
{
	if(allowMovement)
	{
		if(playerColour == tileCollisionNS::PLAYER1_COLOR)
		{
			if (input->isKeyDown(P1_UP_KEY))       // if P1 up arrow
			{
				Entity::update(frameTime);
				spriteData.y += -0.6f;
				spriteData.angle = -90;
			}
			if (input->isKeyDown(P1_DOWN_KEY))     // if P1 down arrow
			{
				Entity::update(frameTime);
				spriteData.y += 0.6f;
				spriteData.angle = 90;
			}
			if (input->isKeyDown(P1_LEFT_KEY))     // if P1 left
			{
				Entity::update(frameTime);
				spriteData.x += -0.6;
				spriteData.angle = -90;
			}
			if (input->isKeyDown(P1_RIGHT_KEY))    // if P1 right
			{
				Entity::update(frameTime);
				spriteData.x += 0.6f;
				spriteData.angle = 0;
			}
		}
		if(playerColour == tileCollisionNS::PLAYER2_COLOR)
		{
			if (input->isKeyDown(P2_UP_KEY))       // if P2 up arrow
			{
				Entity::update(frameTime);
				spriteData.y += -0.6f;
				spriteData.angle = -90;
			}
			if (input->isKeyDown(P2_DOWN_KEY))     // if P2 down arrow
			{
				Entity::update(frameTime);
				spriteData.y += 0.6f;
				spriteData.angle = 90;
			}
			if (input->isKeyDown(P2_LEFT_KEY))     // if P2 left
			{
				Entity::update(frameTime);
				spriteData.x += -0.6f;
				spriteData.angle = -100;
			}
			if (input->isKeyDown(P2_RIGHT_KEY))    // if P2 right
			{
				Entity::update(frameTime);
				spriteData.x += 0.6;
				spriteData.angle = 0;
			}
		}
	}
}
