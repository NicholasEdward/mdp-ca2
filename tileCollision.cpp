// Programming 2D Games
// Copyright (c) 2011,2013 by: 
// Charles Kelly
// tileCollision.cpp v1.0

#include "tileCollision.h"
using namespace tileCollisionNS;

//=============================================================================
// Constructor
//=============================================================================
TileCollision::TileCollision()
{
    mapX = 0;
    mapY = 0;
    menuOn = true;
	messageDialog = NULL;
	questionAsked = false;
	questionAnswered = false;
	questionTimer = 0;
	player1Money = 0;
	player2Money = 0;
	temp = 0;
	goodAns = false;
	badAns = false;
}

//=============================================================================
// Destructor
//=============================================================================
TileCollision::~TileCollision()
{
    releaseAll();           // call onLostDevice() for every graphics item
	SAFE_DELETE(messageDialog);
}

//=============================================================================
// initializes the game
//=============================================================================
void TileCollision::initialize(HWND hwnd)
{
	int randX, randY;

    Game::initialize(hwnd);

    // menu texture
    if (!menuTexture.initialize(graphics,MENU_TEXTURE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menu texture"));

    // tile textures
    if (!tileTextures.initialize(graphics,TILES))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing tile textures"));
	
	  // tile textures
	if (!tileTexturesOverLay.initialize(graphics,OVERLAYTILES))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing pickup textures"));

	  // tile textures
	if (!tileTexturesPickups.initialize(graphics,PICKUPTILES))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing pickup textures"));

    // player texture
    if (!playerTexture.initialize(graphics,PLAYER_TEXTURE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing player texture"));

	// player2 texture
    if (!player2Texture.initialize(graphics,PLAYER2_TEXTURE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing player2 texture"));

	// guard texture
    if (!guardTexture.initialize(graphics,GUARD_TEXTURE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing guard texture"));

    // menu image
    if (!menu.initialize(graphics,0,0,0,&menuTexture))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing menu"));

    // tile image
    if (!tile.initialize(graphics,0,0,0,&tileTextures))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing tile"));

	  // tile image
    if (!pickUp.initialize(graphics,0,0,0,&tileTexturesPickups))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing pickupTiles"));

	 if (!overLay.initialize(graphics,0,0,0,&tileTexturesPickups))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing pickupTiles"));

	// health texture
	if (!healthBarTexture.initialize(graphics,HEALTH_IMAGE))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing game textures"));

    // load map
    mapIn.open(MAP);
    if(!mapIn)
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error loading map data"));
    for(int row=0; row<MAP_HEIGHT; row++)
    {
        for(int col=0; col<MAP_WIDTH; col++)
            mapIn >> tileMap[row][col];
    }

	overLayIn.open(OVERLAY);
    if(!overLayIn)
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error loading overlay data"));
    for(int row=0; row<MAP_HEIGHT; row++)
    {
        for(int col=0; col<MAP_WIDTH; col++)
			overLayIn >> overLayMap[row][col];
    }

	// load Pick ups
	pickUpIn.open(PICKUP);
    if(!pickUpIn)
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error loading pickup data"));
    for(int row=0; row<MAP_HEIGHT; row++)
    {
        for(int col=0; col<MAP_WIDTH; col++)
            pickUpIn >> pickUpMap[row][col];
    }
	mapY = -TILE_SIZE*(MAP_HEIGHT-3);

    // player one
    if (!player.initialize(this,playerNS::WIDTH,playerNS::HEIGHT,playerNS::TEXTURE_COLS,&playerTexture))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing player"));
    player.setFrames(playerNS::START_FRAME, playerNS::END_FRAME);
    player.setCurrentFrame(playerNS::START_FRAME);
    player.setMass(playerNS::MASS);
    player.setDegrees(0);
    // Start player at bottom
    player.setX(static_cast<float>(GAME_WIDTH/2 - player.getWidth()/2));
    player.setY(static_cast<float>(GAME_HEIGHT - player.getHeight()));
	player.playerColour = tileCollisionNS::PLAYER1_COLOR;

	// player two
    if (!player2.initialize(this,playerNS::WIDTH,playerNS::HEIGHT,playerNS::TEXTURE_COLS,&playerTexture))
        throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing player2"));
    player2.setFrames(playerNS::START_FRAME, playerNS::END_FRAME);
    player2.setCurrentFrame(playerNS::START_FRAME);
    player2.setMass(playerNS::MASS);
    player2.setDegrees(0);
    // Start player at bottom
    player2.setX(static_cast<float>(GAME_WIDTH/2 - player.getWidth()/4));
    player2.setY(static_cast<float>(GAME_HEIGHT - player.getHeight()));
	player2.playerColour = tileCollisionNS::PLAYER2_COLOR;

	// guard2
		if (!guard2.initialize(this, guardNS::WIDTH, guardNS::HEIGHT, guardNS::TEXTURE_COLS, &guardTexture))
            throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing ship2"));
        guard2.setFrames(guardNS::START_FRAME, guardNS::END_FRAME);
        guard2.setCurrentFrame(guardNS::START_FRAME);
        guard2.setColorFilter(SETCOLOR_ARGB(255,255,255,64));    // light yellow, used for shield
        guard2.setMass(guardNS::MASS);

    // tileEntity, used for collision testing
    tileEntity.initialize(this,TILE_SIZE,TILE_SIZE,1,&tileTextures);
    tileEntity.setEdge(TILE_EDGE);  // for tile collision
    tileEntity.setCollisionType(entityNS::BOX);
    tileEntity.setMass(TILE_MASS);

		    // overlay entity, used for collision testing
	overLayEntity.initialize(this,TILE_SIZE,TILE_SIZE,1,&tileTexturesOverLay);
    overLayEntity.setEdge(TILE_EDGE);  // for tile collision
    overLayEntity.setCollisionType(entityNS::BOX);
    overLayEntity.setMass(TILE_MASS);
	
	    // pickUpEntity, used for collision testing
	pickUpEntity.initialize(this,TILE_SIZE,TILE_SIZE,1,&tileTexturesPickups);
    pickUpEntity.setEdge(TILE_EDGE);  // for tile collision
    pickUpEntity.setCollisionType(entityNS::BOX);
    pickUpEntity.setMass(TILE_MASS);

	// health bars
	player1HealthBar.initialize(graphics, &healthBarTexture, 0, tileCollisionNS::HEALTHBAR_Y, 2.0f, graphicsNS::WHITE);
	player2HealthBar.initialize(graphics, &healthBarTexture, 0, tileCollisionNS::HEALTHBAR_Y, 2.0f, graphicsNS::WHITE);

	fontScore.initialize(graphics, FONT_SCORE_SIZE, false, false, ::FONT);

    return;
}

//=============================================================================
// Update all game items
//=============================================================================
void TileCollision::update()
{
    float playerX, player2X, playerY, player2Y;
	float guardX, guardY;


    if (menuOn)
    {
        if (input->anyKeyPressed())
        {
            menuOn = false;
            input->clearAll();
        }
    }
	//Asking Question Logic (Nick)
	//If a question is being asked, and has not yet been answered
	if((questionAsked) && (!questionAnswered))
	{
		//Call the ask Question method with a null reference until it has completed
		askQuestion(NULL);
		messageDialog->update();    // check for button events
		if(!questionAnswered)
		{
			if(messageDialog->getButtonClicked() == 1)
			{
				badAns = true;
			}
			else if(messageDialog->getButtonClicked() == 2)
			{
				goodAns = true;
			}
			if(badAns)
			{
				negResponse();
			}
			if(goodAns)
			{
				posResponse();
			}
		}
	}
	//If the question has been answered, then allow the player and guard to progress
	//Begin the question timer so that a guard cannot ask you another question for at least 10 seconds
	if(questionAnswered)
	{
		questionAsked = false;
		player.allowMovement = true;
		player2.allowMovement = true;
		guard2.allowMovement = true;

		questionTimer += frameTime;
		if(questionTimer >= 10)
		{
			questionTimer = 0;
			questionAnswered = false;
		}
	}
	player.update(frameTime);
	player2.update(frameTime);
	guard2.update(frameTime);

    tileEntity.update(frameTime);
	overLayEntity.update(frameTime);
	pickUpEntity.update(frameTime);



    // Scroll map left and right
    playerX = player.getX();
	player2X = player2.getX();
	if(playerX < TILE_SIZE*2)         // if player near left edge
    {
        mapX += 1; // scroll map right
        player.setX(TILE_SIZE*2);     // reposition player
		player2.setX(player2.getX() + 1);
		guard2.setX(guard2.getX() + 1);
    }
    // if player near right edge
    else if(playerX > GAME_WIDTH-TILE_SIZE*3)
    {
		 mapX -= 1; 
		 player.setX(GAME_WIDTH-TILE_SIZE*3);
		 player2.setX(player2.getX() - 1);
		 guard2.setX(guard2.getX() - 1);
    }
	if(player2X < TILE_SIZE*2)         // if player2 near left edge
    {
        mapX += 1; // scroll map right
        player2.setX(TILE_SIZE*2);     // reposition player2
		player.setX(player.getX() + 1);
		guard2.setX(guard2.getX() + 1);
    }
    // if player2 near right edge
    else if(player2X > GAME_WIDTH-TILE_SIZE*3)
    {
		 mapX -= 1;
		 player2.setX(GAME_WIDTH-TILE_SIZE*3);
		 player.setX(player.getX() - 1);
		 guard2.setX(guard2.getX() - 1);
		 mapX -=1; 
    }

    // Scroll map up and down
    playerY = player.getY();
	player2Y = player2.getY();
    if(playerY < TILE_SIZE*2)         // if player near top
    {
        mapY += TILE_SIZE*2-playerY;  // scroll map down
        player.setY((GAME_HEIGHT - GAME_HEIGHT) + TILE_SIZE*2);     // reposition player
		player2.setY(player2.getY() + 1);     // reposition player2
		guard2.setY(guard2.getY() + 1);
    }
    // if player near bottom
    else if(playerY > GAME_HEIGHT-TILE_SIZE*3)
    {
        mapY -= playerY-(GAME_HEIGHT-TILE_SIZE*3);    // scroll map up
		player.setY(GAME_HEIGHT-TILE_SIZE*3);   // reposition player
		player2.setY(player2.getY()- 1);   // reposition player2
		guard2.setY(guard2.getY() - 1);
    }
	if(player2Y < TILE_SIZE*2)         // if player near top
    {
        mapY += TILE_SIZE*2-player2Y;  // scroll map down
		player2.setY((GAME_HEIGHT - GAME_HEIGHT) + TILE_SIZE*2);     // reposition player2
		player.setY(player.getY() + 1);     // reposition player
		guard2.setY(guard2.getY() + 1);
    }
    // if player near bottom
    else if(player2Y > GAME_HEIGHT-TILE_SIZE*3)
    {
        mapY -= player2Y-(GAME_HEIGHT-TILE_SIZE*3);    // scroll map up
		player2.setY(player2.getY()-(player2Y-(GAME_HEIGHT-TILE_SIZE*3)));   // reposition player2
		player.setY(player.getY() - 1);   // reposition player
		guard2.setY(guard2.getY() - 1);
    }


    if(mapX > TILE_SIZE*2)  // if map past left edge
    {
        mapX = TILE_SIZE*2; // stop at left edge of map
    }
    // if map past right edge
    else if(mapX < (-MAP_WIDTH * TILE_SIZE) + GAME_WIDTH-TILE_SIZE*2)
    {
        // stop at right edge of map
        mapX = (-MAP_WIDTH * TILE_SIZE) + GAME_WIDTH-TILE_SIZE*2;
        player.setVelocityX(0);  // stop player
		player2.setVelocityX(0);  // stop player
		guard2.turnAround = true;
	}
    if(mapY > GAME_HEIGHT/2)  // if map top below screen middle
    {
        mapY = GAME_HEIGHT/2;  // limit Y
    }
    // if map bottom above screen middle
    else if(mapY < (-MAP_HEIGHT * TILE_SIZE) + GAME_HEIGHT/2)
    {
        // limit Y
        mapY = (-MAP_HEIGHT * TILE_SIZE) + GAME_HEIGHT/2;
    }

	
}

//=============================================================================
// Artificial Intelligence
//=============================================================================
void TileCollision::ai()
{}

//=============================================================================
// Handle collisions
// An Entity object (tileEntity) is used to represent a game tile. That allows
// using the standard collision tests in the game engine. The tile map is named
// tileMap. It holds a number representing the type of the tile in each grid.
// The tile numbers that are less than or equal to MAX_COLLISION_TILE may be
// collided with. All other tile numbers do not collide.
//
// The fist thing this collision code does is calculate the current row and
// column of the player. The map can scroll. The amount the map has scrolled is
// contained in mapX and mapY. A collision test is performed between the player
// and the nearby tiles. The player's position is defined by its top left corner
// so we don't need to check for collisions with any tiles that are above the
// current row. The possible collision tiles is the tile to the left, the
// current tile, the tile to the right and the three closest tiles in the row
// below the player. The screen position of each of those tiles is calculated and
// used to set the parameters of tileEntity. A standard collision test is used
// between the player and tileEntity. The tileEntity object is reused for each
// tile.
//=============================================================================
void TileCollision::collisionTiles()
{
    VECTOR2 collisionVector, guardCollisionVector;

    // Calculate map grid location of player
    UINT playerRow = static_cast<UINT>((player.getY() - mapY)/TILE_SIZE);
    UINT playerCol = static_cast<UINT>((player.getX() - mapX)/TILE_SIZE);
	UINT playerRow2 = static_cast<UINT>((player2.getY() - mapY)/TILE_SIZE);
    UINT playerCol2 = static_cast<UINT>((player2.getX() - mapX)/TILE_SIZE);

    // check for collision with left, current, right, bottom left, bottom and bottom right tiles.
    // if tile left of player may be collided with
    if(playerCol > 0 && playerRow < MAP_HEIGHT && tileMap[playerRow][playerCol-1] <= MAX_COLLISION_TILE)
    {
        tileEntity.setX(static_cast<float>((playerCol-1)*TILE_SIZE+mapX));    // position tileEntity
        tileEntity.setY(static_cast<float>(playerRow*TILE_SIZE+mapY));
        tileEntity.setRotatedBoxReady(false);        
        // if collision between player and tile
        if(player.collidesWith(tileEntity, collisionVector))
            player.bounce(collisionVector,tileEntity);
    }
    // if current tile may be collided with
    if(playerCol >= 0 && playerRow < MAP_HEIGHT && tileMap[playerRow][playerCol] <= MAX_COLLISION_TILE)
    {
        tileEntity.setX(static_cast<float>(playerCol*TILE_SIZE+mapX));     // position tileEntity
        tileEntity.setY(static_cast<float>(playerRow*TILE_SIZE+mapY));
        tileEntity.setRotatedBoxReady(false);        
        // if collision between player and tile
        if(player.collidesWith(tileEntity, collisionVector))
            player.bounce(collisionVector,tileEntity);
    }
    // if tile right of player may be collided with
    if(playerCol < MAP_WIDTH-1 && playerRow < MAP_HEIGHT && tileMap[playerRow][playerCol+1] <= MAX_COLLISION_TILE)
    {
        tileEntity.setX(static_cast<float>((playerCol+1)*TILE_SIZE+mapX));   // position tileEntity
        tileEntity.setY(static_cast<float>(playerRow*TILE_SIZE+mapY));
        tileEntity.setRotatedBoxReady(false);        
        // if collision between player and tile
        if(player.collidesWith(tileEntity, collisionVector))
            player.bounce(collisionVector,tileEntity);
    }
    // if tile bottom left of player may be collided with
    if(playerCol > 0 && playerRow < MAP_HEIGHT-1 && tileMap[playerRow+1][playerCol-1] <= MAX_COLLISION_TILE)
    {
        tileEntity.setX(static_cast<float>((playerCol-1)*TILE_SIZE+mapX));   // position tileEntity
        tileEntity.setY(static_cast<float>((playerRow+1)*TILE_SIZE+mapY));
        tileEntity.setRotatedBoxReady(false);        
        // if collision between player and tile
        if(player.collidesWith(tileEntity, collisionVector))
            player.bounce(collisionVector,tileEntity);
    }
    // if tile below player may be collided with
    if(playerRow < MAP_HEIGHT-1 && tileMap[playerRow+1][playerCol] <= MAX_COLLISION_TILE)
    {
        tileEntity.setX(static_cast<float>(playerCol*TILE_SIZE+mapX));     // position tileEntity
        tileEntity.setY(static_cast<float>((playerRow+1)*TILE_SIZE+mapY));
        tileEntity.setRotatedBoxReady(false);        
        // if collision between player and tile
        if(player.collidesWith(tileEntity, collisionVector))
            player.bounce(collisionVector,tileEntity);
    }
    // if tile bottom right of player may be collided with
    if(playerCol < MAP_WIDTH-1 && playerRow < MAP_HEIGHT-1 && tileMap[playerRow+1][playerCol+1] <= MAX_COLLISION_TILE)
    {
        tileEntity.setX(static_cast<float>((playerCol+1)*TILE_SIZE+mapX));   // position tileEntity
        tileEntity.setY(static_cast<float>((playerRow+1)*TILE_SIZE+mapY));
        tileEntity.setRotatedBoxReady(false);        
        // if collision between player and tile
        if(player.collidesWith(tileEntity, collisionVector))
            player.bounce(collisionVector,tileEntity);
    }
	// if collision between player and guard
	if(player.collidesWith(guard2, collisionVector))
	{
		//if you collide with a guard, he will ask you a question
		questionAsked = true;
		//if a question is currently being asked, then the guard and the player will not be able to move
		guard2.allowMovement = false;
		player.allowMovement = false;
		player.bounce(collisionVector,guard2);
	}
	// check for collision with left, current, right, bottom left, bottom and bottom right tiles.
    // if tile left of player may be collided with PLAYER TWO COLLISION
    if(playerCol2 > 0 && playerRow2 < MAP_HEIGHT && tileMap[playerRow2][playerCol2-1] <= MAX_COLLISION_TILE)
    {
        tileEntity.setX(static_cast<float>((playerCol2-1)*TILE_SIZE+mapX));    // position tileEntity
        tileEntity.setY(static_cast<float>(playerRow2*TILE_SIZE+mapY));
        tileEntity.setRotatedBoxReady(false);        
        // if collision between player and tile
        if(player2.collidesWith(tileEntity, collisionVector))
            player2.bounce(collisionVector,tileEntity);
    }
    // if current tile may be collided with
    if(playerCol2 >= 0 && playerRow2 < MAP_HEIGHT && tileMap[playerRow2][playerCol2] <= MAX_COLLISION_TILE)
    {
        tileEntity.setX(static_cast<float>(playerCol2*TILE_SIZE+mapX));     // position tileEntity
        tileEntity.setY(static_cast<float>(playerRow2*TILE_SIZE+mapY));
        tileEntity.setRotatedBoxReady(false);        
        // if collision between player and tile
        if(player2.collidesWith(tileEntity, collisionVector))
            player2.bounce(collisionVector,tileEntity);
    }
    // if tile right of player may be collided with
    if(playerCol2 < MAP_WIDTH-1 && playerRow2 < MAP_HEIGHT && tileMap[playerRow2][playerCol2+1] <= MAX_COLLISION_TILE)
    {
        tileEntity.setX(static_cast<float>((playerCol2+1)*TILE_SIZE+mapX));   // position tileEntity
        tileEntity.setY(static_cast<float>(playerRow2*TILE_SIZE+mapY));
        tileEntity.setRotatedBoxReady(false);        
        // if collision between player and tile
        if(player2.collidesWith(tileEntity, collisionVector))
            player2.bounce(collisionVector,tileEntity);
    }
    // if tile bottom left of player may be collided with
    if(playerCol2 > 0 && playerRow2 < MAP_HEIGHT-1 && tileMap[playerRow2+1][playerCol2-1] <= MAX_COLLISION_TILE)
    {
        tileEntity.setX(static_cast<float>((playerCol2-1)*TILE_SIZE+mapX));   // position tileEntity
        tileEntity.setY(static_cast<float>((playerRow2+1)*TILE_SIZE+mapY));
        tileEntity.setRotatedBoxReady(false);        
        // if collision between player and tile
        if(player2.collidesWith(tileEntity, collisionVector))
            player2.bounce(collisionVector,tileEntity);
    }
    // if tile below player may be collided with
    if(playerRow2 < MAP_HEIGHT-1 && tileMap[playerRow2+1][playerCol2] <= MAX_COLLISION_TILE)
    {
        tileEntity.setX(static_cast<float>(playerCol2*TILE_SIZE+mapX));     // position tileEntity
        tileEntity.setY(static_cast<float>((playerRow2+1)*TILE_SIZE+mapY));
        tileEntity.setRotatedBoxReady(false);        
        // if collision between player and tile
        if(player2.collidesWith(tileEntity, collisionVector))
            player2.bounce(collisionVector,tileEntity);
    }
    // if tile bottom right of player may be collided with
    if(playerCol2 < MAP_WIDTH-1 && playerRow2 < MAP_HEIGHT-1 && tileMap[playerRow2+1][playerCol2+1] <= MAX_COLLISION_TILE)
    {
        tileEntity.setX(static_cast<float>((playerCol2+1)*TILE_SIZE+mapX));   // position tileEntity
        tileEntity.setY(static_cast<float>((playerRow2+1)*TILE_SIZE+mapY));
        tileEntity.setRotatedBoxReady(false);        
        // if collision between player and tile
        if(player2.collidesWith(tileEntity, collisionVector))
            player2.bounce(collisionVector,tileEntity);
    }
	// if collision between player and guard
	if(player2.collidesWith(guard2, collisionVector))
	{
		//if you collide with a guard, he will ask you a question
		questionAsked = true;
		//if a question is currently being asked, then the guard and the player will not be able to move
		guard2.allowMovement = false;
		player2.allowMovement = false;
		player2.bounce(collisionVector,guard2);
	}
	//Guard Collisions
	// check for collision with left, current, right, bottom left, bottom and bottom right tiles.
    // if tile left of guard may be collided with
	if(!guard2.turnAround)
	{
		UINT guardRow = static_cast<UINT>((guard2.getY() - mapY)/TILE_SIZE);
		UINT guardCol = static_cast<UINT>((guard2.getX() - mapX)/TILE_SIZE);
		if(guardCol > 0 && guardRow < MAP_HEIGHT && tileMap[guardRow][guardCol-1] <= MAX_COLLISION_TILE)
		{
			tileEntity.setX(static_cast<float>((guardCol-1)*TILE_SIZE+mapX));    // position tileEntity
			tileEntity.setY(static_cast<float>(guardRow*TILE_SIZE+mapY));
			tileEntity.setRotatedBoxReady(false);        
			// if collision between guard and tile
			if(guard2.collidesWith(tileEntity, guardCollisionVector))
			{
				guard2.bounce(guardCollisionVector, tileEntity);
				guard2.turnAround = true;
			}
		}
		// if current tile may be collided with
		if(guardCol >= 0 && guardRow < MAP_HEIGHT && tileMap[guardRow][guardCol] <= MAX_COLLISION_TILE)
		{
			tileEntity.setX(static_cast<float>(guardCol*TILE_SIZE+mapX));     // position tileEntity
			tileEntity.setY(static_cast<float>(guardRow*TILE_SIZE+mapY));
			tileEntity.setRotatedBoxReady(false);        
			// if collision between guard and tile
			if(guard2.collidesWith(tileEntity, collisionVector))
			{
				guard2.bounce(collisionVector, tileEntity);
				guard2.turnAround = true;
			}
		}
		// if tile right of guard may be collided with
		if(guardCol < MAP_WIDTH-1 && guardRow < MAP_HEIGHT && tileMap[guardRow][guardCol+1] <= MAX_COLLISION_TILE)
		{
			tileEntity.setX(static_cast<float>((guardCol+1)*TILE_SIZE+mapX));   // position tileEntity
			tileEntity.setY(static_cast<float>(guardRow*TILE_SIZE+mapY));
			tileEntity.setRotatedBoxReady(false);        
			// if collision between guard and tile
			if(guard2.collidesWith(tileEntity, collisionVector))
			{
				guard2.bounce(collisionVector, tileEntity);
				guard2.turnAround = true;
			}
		}
		// if tile bottom left of guard may be collided with
		if(guardCol > 0 && guardRow < MAP_HEIGHT-1 && tileMap[guardRow+1][guardCol-1] <= MAX_COLLISION_TILE)
		{
			tileEntity.setX(static_cast<float>((guardCol-1)*TILE_SIZE+mapX));   // position tileEntity
			tileEntity.setY(static_cast<float>((guardRow+1)*TILE_SIZE+mapY));
			tileEntity.setRotatedBoxReady(false);        
			// if collision between guard and tile
			if(guard2.collidesWith(tileEntity, collisionVector))
			{
				guard2.bounce(collisionVector, tileEntity);
				guard2.turnAround = true;
			}
		}
		// if tile below guard may be collided with
		if(guardRow < MAP_HEIGHT-1 && tileMap[guardRow+1][guardCol] <= MAX_COLLISION_TILE)
		{
			tileEntity.setX(static_cast<float>(guardCol*TILE_SIZE+mapX));     // position tileEntity
			tileEntity.setY(static_cast<float>((guardRow+1)*TILE_SIZE+mapY));
			tileEntity.setRotatedBoxReady(false);        
			// if collision between guard and tile
			if(guard2.collidesWith(tileEntity, collisionVector))
			{
				guard2.bounce(collisionVector, tileEntity);
				guard2.turnAround = true;
			}
		}
		// if tile bottom right of guard may be collided with
		if(guardCol < MAP_WIDTH-1 && guardRow < MAP_HEIGHT-1 && tileMap[guardRow+1][guardCol+1] <= MAX_COLLISION_TILE)
		{
			tileEntity.setX(static_cast<float>((guardCol+1)*TILE_SIZE+mapX));   // position tileEntity
			tileEntity.setY(static_cast<float>((guardRow+1)*TILE_SIZE+mapY));
			tileEntity.setRotatedBoxReady(false);        
			// if collision between guard and tile
			if(guard2.collidesWith(tileEntity, collisionVector))
			{
				guard2.bounce(collisionVector, tileEntity);
				guard2.turnAround = true;
			}
		}
	}
	if(guard2.turnAround)
	{
		guard2.directionModifier * -1;
		guard2.turnAround = false;
	}
}

void TileCollision::collisionOverLays()
{
    VECTOR2 collisionVector;

    // Calculate map grid location of player
    UINT playerRow = static_cast<UINT>((player.getY() - mapY)/TILE_SIZE);
    UINT playerCol = static_cast<UINT>((player.getX() - mapX)/TILE_SIZE);

    // check for collision with left, current, right, bottom left, bottom and bottom right tiles.
    // if tile left of player may be collided with
    if(playerCol > 0 && playerRow < MAP_HEIGHT && overLayMap[playerRow][playerCol-1] <= MAX_COLLISION_TILE && overLayMap[playerRow][playerCol-1] != 0)
    {
        overLayEntity.setX(static_cast<float>((playerCol-1)*TILE_SIZE+mapX));    // position overLayEntity
        overLayEntity.setY(static_cast<float>(playerRow*TILE_SIZE+mapY));
        overLayEntity.setRotatedBoxReady(false);        
        // if collision between player and tile
        if(player.collidesWith(overLayEntity, collisionVector))
            player.bounce(collisionVector,overLayEntity);
    }
    // if current tile may be collided with
    if(playerCol >= 0 && playerRow < MAP_HEIGHT && overLayMap[playerRow][playerCol] <= MAX_COLLISION_TILE && overLayMap[playerRow][playerCol] != 0)
    {
        overLayEntity.setX(static_cast<float>(playerCol*TILE_SIZE+mapX));     // position overLayEntity
        overLayEntity.setY(static_cast<float>(playerRow*TILE_SIZE+mapY));
        overLayEntity.setRotatedBoxReady(false);        
        // if collision between player and tile
        if(player.collidesWith(overLayEntity, collisionVector))
            player.bounce(collisionVector,overLayEntity);
    }
    // if tile right of player may be collided with
    if(playerCol < MAP_WIDTH-1 && playerRow < MAP_HEIGHT && overLayMap[playerRow][playerCol+1] <= MAX_COLLISION_TILE && overLayMap[playerRow][playerCol+1] != 0)
    {
        overLayEntity.setX(static_cast<float>((playerCol+1)*TILE_SIZE+mapX));   // position overLayEntity
        overLayEntity.setY(static_cast<float>(playerRow*TILE_SIZE+mapY));
        overLayEntity.setRotatedBoxReady(false);        
        // if collision between player and tile
        if(player.collidesWith(overLayEntity, collisionVector))
            player.bounce(collisionVector,overLayEntity);
    }
    // if tile bottom left of player may be collided with
    if(playerCol > 0 && playerRow < MAP_HEIGHT-1 && overLayMap[playerRow+1][playerCol-1] <= MAX_COLLISION_TILE && overLayMap[playerRow+1][playerCol-1] != 0)
    {
        overLayEntity.setX(static_cast<float>((playerCol-1)*TILE_SIZE+mapX));   // position overLayEntity
        overLayEntity.setY(static_cast<float>((playerRow+1)*TILE_SIZE+mapY));
        overLayEntity.setRotatedBoxReady(false);        
        // if collision between player and tile
        if(player.collidesWith(overLayEntity, collisionVector))
            player.bounce(collisionVector,overLayEntity);
    }
    // if tile below player may be collided with
    if(playerRow < MAP_HEIGHT-1 && overLayMap[playerRow+1][playerCol] <= MAX_COLLISION_TILE && overLayMap[playerRow+1][playerCol] != 0)
    {
        overLayEntity.setX(static_cast<float>(playerCol*TILE_SIZE+mapX));     // position overLayEntity
        overLayEntity.setY(static_cast<float>((playerRow+1)*TILE_SIZE+mapY));
        overLayEntity.setRotatedBoxReady(false);        
        // if collision between player and tile
        if(player.collidesWith(overLayEntity, collisionVector))
            player.bounce(collisionVector,overLayEntity);
    }
    // if tile bottom right of player may be collided with
    if(playerCol < MAP_WIDTH-1 && playerRow < MAP_HEIGHT-1 && overLayMap[playerRow+1][playerCol+1] <= MAX_COLLISION_TILE && overLayMap[playerRow+1][playerCol+1] != 0)
    {
        overLayEntity.setX(static_cast<float>((playerCol+1)*TILE_SIZE+mapX));   // position overLayEntity
        overLayEntity.setY(static_cast<float>((playerRow+1)*TILE_SIZE+mapY));
        overLayEntity.setRotatedBoxReady(false);        
        // if collision between player and tile
        if(player.collidesWith(overLayEntity, collisionVector))
            player.bounce(collisionVector,overLayEntity);
    }
}

void TileCollision::collisionPickUps()
{
    VECTOR2 collisionVector;
    // Calculate map grid location of player
    UINT playerRow = static_cast<UINT>((player.getY() - mapY)/TILE_SIZE);
    UINT playerCol = static_cast<UINT>((player.getX() - mapX)/TILE_SIZE);
	UINT playerRow2 = static_cast<UINT>((player2.getY() - mapY)/TILE_SIZE);
    UINT playerCol2 = static_cast<UINT>((player2.getX() - mapX)/TILE_SIZE);
    // check for collision with left, current, right, bottom left, bottom and bottom right tiles.

    // if tile left of player may be collided with
    if(playerCol > 0 && playerRow < MAP_HEIGHT && pickUpMap[playerRow][playerCol-1] != 0)
    {
		pickUpEntity.setX(static_cast<float>((playerCol-1)*TILE_SIZE+mapX));    // position tileEntity
        pickUpEntity.setY(static_cast<float>(playerRow*TILE_SIZE+mapY));
        pickUpEntity.setRotatedBoxReady(false);        
        // if collision between player and tile
        if(player.collidesWith(pickUpEntity, collisionVector))
		{
			
				pickupInteraction(playerRow, playerCol);
		}
    }
    // if current tile may be collided with
    if(playerCol >= 0 && playerRow < MAP_HEIGHT && pickUpMap[playerRow][playerCol] != 0)
    {
        pickUpEntity.setX(static_cast<float>(playerCol*TILE_SIZE+mapX));     // position tileEntity
        pickUpEntity.setY(static_cast<float>(playerRow*TILE_SIZE+mapY));
        pickUpEntity.setRotatedBoxReady(false);        
        // if collision between player and tile
       if(player.collidesWith(pickUpEntity, collisionVector))
		{
			
				pickupInteraction(playerRow, playerCol);
		}
    }
    // if tile right of player may be collided with
    if(playerCol < MAP_WIDTH-1 && playerRow < MAP_HEIGHT && pickUpMap[playerRow][playerCol+1] != 0)
    {
        pickUpEntity.setX(static_cast<float>((playerCol+1)*TILE_SIZE+mapX));   // position tileEntity
        pickUpEntity.setY(static_cast<float>(playerRow*TILE_SIZE+mapY));
        pickUpEntity.setRotatedBoxReady(false);        
        // if collision between player and tile
      if(player.collidesWith(pickUpEntity, collisionVector))
		{
				pickupInteraction(playerRow, playerCol+1);
		}
    }
    // if tile bottom left of player may be collided with
    if(playerCol > 0 && playerRow < MAP_HEIGHT-1 && pickUpMap[playerRow+1][playerCol-1] != 0)
    {
        pickUpEntity.setX(static_cast<float>((playerCol-1)*TILE_SIZE+mapX));   // position tileEntity
        pickUpEntity.setY(static_cast<float>((playerRow+1)*TILE_SIZE+mapY));
        pickUpEntity.setRotatedBoxReady(false);        
        // if collision between player and tile
       if(player.collidesWith(pickUpEntity, collisionVector))
		{
				pickupInteraction(playerRow, playerCol);
		}
    }
    // if tile below player may be collided with
    if(playerRow < MAP_HEIGHT-1 && pickUpMap[playerRow+1][playerCol] != 0)
    {
        pickUpEntity.setX(static_cast<float>(playerCol*TILE_SIZE+mapX));     // position tileEntity
        pickUpEntity.setY(static_cast<float>((playerRow+1)*TILE_SIZE+mapY));
        pickUpEntity.setRotatedBoxReady(false);        
        // if collision between player and tile
       if(player.collidesWith(pickUpEntity, collisionVector))
		{

				pickupInteraction(playerRow, playerCol);
		}
    }
    // if tile bottom right of player may be collided with
    if(playerCol < MAP_WIDTH-1 && playerRow < MAP_HEIGHT-1 && pickUpMap[playerRow+1][playerCol+1] != 0)
    {
        pickUpEntity.setX(static_cast<float>((playerCol+1)*TILE_SIZE+mapX));   // position tileEntity
        pickUpEntity.setY(static_cast<float>((playerRow+1)*TILE_SIZE+mapY));
        pickUpEntity.setRotatedBoxReady(false);        
        // if collision between player and tile
       if(player.collidesWith(pickUpEntity, collisionVector))
		{

				pickupInteraction(playerRow, playerCol);
		}
    }
	if(playerCol > 0 && playerRow < MAP_HEIGHT && pickUpMap[playerRow][playerCol-1])
    {
		pickUpEntity.setX(static_cast<float>((playerCol-1)*TILE_SIZE+mapX));    // position tileEntity
        pickUpEntity.setY(static_cast<float>(playerRow*TILE_SIZE+mapY));
        pickUpEntity.setRotatedBoxReady(false);        
        // if collision between player and tile
        if(player.collidesWith(pickUpEntity, collisionVector))
		{
            player.bounce(collisionVector,pickUpEntity);
			if(pickUpMap[playerRow][playerCol] == 7)
			{
				pickUpMap[playerRow][playerCol] = 8;
			}
			
		}
    }
    // if current tile may be collided with
    if(playerCol >= 0 && playerRow < MAP_HEIGHT && pickUpMap[playerRow][playerCol])
    {
        pickUpEntity.setX(static_cast<float>(playerCol*TILE_SIZE+mapX));     // position tileEntity
        pickUpEntity.setY(static_cast<float>(playerRow*TILE_SIZE+mapY));
        pickUpEntity.setRotatedBoxReady(false);        
        // if collision between player and tile
       if(player.collidesWith(pickUpEntity, collisionVector))
		{
            player.bounce(collisionVector,pickUpEntity);
			if(pickUpMap[playerRow][playerCol] == 7)
			{
				pickUpMap[playerRow][playerCol] = 8;
			}
			
		}
    }
    // if tile right of player may be collided with
    if(playerCol < MAP_WIDTH-1 && playerRow < MAP_HEIGHT && pickUpMap[playerRow][playerCol+1])
    {
        pickUpEntity.setX(static_cast<float>((playerCol+1)*TILE_SIZE+mapX));   // position tileEntity
        pickUpEntity.setY(static_cast<float>(playerRow*TILE_SIZE+mapY));
        pickUpEntity.setRotatedBoxReady(false);        
        // if collision between player and tile
      if(player.collidesWith(pickUpEntity, collisionVector))
		{
            player.bounce(collisionVector,pickUpEntity);
			if(pickUpMap[playerRow][playerCol] == 7)
			{
				pickUpMap[playerRow][playerCol] = 8;
			}
			
		}
    }
    // if tile bottom left of player may be collided with
    if(playerCol > 0 && playerRow < MAP_HEIGHT-1 && pickUpMap[playerRow+1][playerCol-1])
    {
        pickUpEntity.setX(static_cast<float>((playerCol-1)*TILE_SIZE+mapX));   // position tileEntity
        pickUpEntity.setY(static_cast<float>((playerRow+1)*TILE_SIZE+mapY));
        pickUpEntity.setRotatedBoxReady(false);        
        // if collision between player and tile
       if(player.collidesWith(pickUpEntity, collisionVector))
		{
            player.bounce(collisionVector,pickUpEntity);
			if(pickUpMap[playerRow][playerCol] == 7)
			{
				pickUpMap[playerRow][playerCol] = 8;
			}
			
		}
    }
    // if tile below player may be collided with
    if(playerRow < MAP_HEIGHT-1 && pickUpMap[playerRow+1][playerCol])
    {
        pickUpEntity.setX(static_cast<float>(playerCol2*TILE_SIZE+mapX));     // position tileEntity
        pickUpEntity.setY(static_cast<float>((playerRow2+1)*TILE_SIZE+mapY));
        pickUpEntity.setRotatedBoxReady(false);        
        // if collision between player and tile
       if(player2.collidesWith(pickUpEntity, collisionVector))
		{
            player2.bounce(collisionVector,pickUpEntity);
			if(pickUpMap[playerRow2][playerCol2] == 7)
			{
				pickUpMap[playerRow2][playerCol2] = 8;
			}
		}
    }
    // if tile bottom right of player may be collided with
    if(playerCol2 < MAP_WIDTH-1 && playerRow2 < MAP_HEIGHT-1 && pickUpMap[playerRow2+1][playerCol2+1])
    {
        pickUpEntity.setX(static_cast<float>((playerCol2+1)*TILE_SIZE+mapX));   // position tileEntity
        pickUpEntity.setY(static_cast<float>((playerRow2+1)*TILE_SIZE+mapY));
        pickUpEntity.setRotatedBoxReady(false);        
        // if collision between player and tile
       if(player2.collidesWith(pickUpEntity, collisionVector))
		{
            player2.bounce(collisionVector,pickUpEntity);
			if(pickUpMap[playerRow2][playerCol2] == 7)
			{
				pickUpMap[playerRow2][playerCol2] = 8;
			}
			
		}
    }
}

void TileCollision::pickupInteraction(int row, int col)
{
	srand(time(NULL)); // Seed a random number with time
	randomMoney = rand() % 10 + 1; // Get a random number between 1 + 10
	switch (pickUpMap[row][col])
	{
	case 51: console->print("MONEY");
		player1Money += randomMoney;
		player2Money += randomMoney;
		audio->playCue("pickup");
		break;
	case 43: console->print("SHOVEL");
		audio->playCue("shovel");
		break;
	default: console->print("OTHER");
		audio->playCue("pickup");
		break;
	}
	pickUpMap[row][col] = 0;
}

void TileCollision::askQuestion(const char theQuestion[])
{
	srand(time(NULL)); // Seed a random number with time
	randomIndex = rand() % 10 + 1; // Get a random number between 1 + 10

	//Switch statement to change the question that is printed out
	switch (randomIndex)
	{
	default: theQuestion = messageDialogNS::DEFAULT_QUESTION; break;
	case 1: theQuestion = messageDialogNS::QUESTION1; break;
	case 2: theQuestion = messageDialogNS::QUESTION2; break;
	case 3: theQuestion = messageDialogNS::QUESTION3; break;
	case 4: theQuestion = messageDialogNS::QUESTION4; break;
	case 5: theQuestion = messageDialogNS::QUESTION5; break;
	case 6: theQuestion = messageDialogNS::QUESTION6; break;
	case 7: theQuestion = messageDialogNS::QUESTION7; break;
	case 8: theQuestion = messageDialogNS::QUESTION8; break;
	case 9: theQuestion = messageDialogNS::QUESTION9; break;
	case 10: theQuestion = messageDialogNS::QUESTION10; break;
	}
	messageDialog->setBorderColor(graphicsNS::ALPHA25);
    messageDialog->setBackColor(SETCOLOR_ARGB(0,0,0,0));
	//Set the randomly chosen question and print it
	messageDialog->print(theQuestion);
}
void TileCollision::posResponse()
{
	messageDialog->setBorderColor(graphicsNS::ALPHA25);
	messageDialog->setBackColor(tileCollisionNS::PLAYER1_COLOR);
	//Set the randomly chosen question and print it
	messageDialog->print(messageDialogNS::POSITIVERESPONSE);
	temp++;
	srand(time(NULL)); // Seed a random number with time
	randomMoney = rand() % 10 + 1; // Get a random number between 1 + 10
	player1Money -= randomMoney;
	player2Money -= randomMoney;
	audio->playCue("cash");
	goodAns = false;
	questionAnswered = true;
}
void TileCollision::negResponse()
{
	messageDialog->setBorderColor(graphicsNS::ALPHA25);
    messageDialog->setBackColor(tileCollisionNS::PLAYER2_COLOR);
	//Set the randomly chosen question and print it
	messageDialog->print(messageDialogNS::NEGATIVERESPONSE);
	temp++;

	srand(time(NULL)); // Seed a random number with time
	randomDamage = rand() % 10 + 1; // Get a random number between 1 + 10
	player.loseHealth(randomDamage);
	player2.loseHealth(randomDamage);
	audio->playCue("hurt");
	badAns = false;
	questionAnswered = true;
}

//=============================================================================
// Render game items
//=============================================================================
void TileCollision::render()
{
    graphics->spriteBegin();

    for(int row=0; row<MAP_HEIGHT; row++)       // for each row of map
    {
        tile.setY( (float)(row*TILE_SIZE) ); // set tile Y
        pickUp.setY( (float)(row*TILE_SIZE) ); // set tile Y
        overLay.setY( (float)(row*TILE_SIZE) ); // set tile Y

		
        for(int col=0; col<MAP_WIDTH; col++)    // for each column of map
        {
            if(tileMap[row][col] >= 0)          // if tile present
            {
                tile.setX( (float)(col*(TILE_SIZE)) + mapX );  // set tile X
                tile.setY( (float)(row*(TILE_SIZE)) + mapY );  // set tile Y
                // if tile on screen
                if( (tile.getX() > -TILE_SIZE && tile.getX() < GAME_WIDTH) &&
                    (tile.getY() > -TILE_SIZE && tile.getY() < GAME_HEIGHT) )
                    tile.draw(UINT(tileMap[row][col]));             // draw tile
            }

			if(overLayMap[row][col] > 0)          // if tile present
            {
                overLay.setX( (float)(col*(TILE_SIZE)) + mapX );  // set tile X
                overLay.setY( (float)(row*(TILE_SIZE)) + mapY );  // set tile Y
                // if tile on screen
                if( (overLay.getX() > -TILE_SIZE && overLay.getX() < GAME_WIDTH) &&
					(overLay.getY() > -TILE_SIZE && overLay.getY() < GAME_HEIGHT) )
					 overLay.draw(UINT(overLayMap[row][col]));      
            }

			if(pickUpMap[row][col] > 0)          // if tile present
            {
				pickUp.setX( (float)(col*(TILE_SIZE)) + mapX );  // set tile X
                pickUp.setY( (float)(row*(TILE_SIZE)) + mapY );  // set tile Y
                // if tile on screen
                if( (pickUp.getX() > -TILE_SIZE && pickUp.getX() < GAME_WIDTH) &&
                    (pickUp.getY() > -TILE_SIZE && pickUp.getY() < GAME_HEIGHT) )
				{
					  pickUp.draw(UINT(pickUpMap[row][col]));
					
				}

					
            }
        }
    }

	// display health bars
	player1HealthBar.setX((float)tileCollisionNS::PLAYER1_HEALTHBAR_X);
    player1HealthBar.set(player.getHealth());

	player2HealthBar.setX((float)tileCollisionNS::PLAYER2_HEALTHBAR_X);
    player2HealthBar.set(player.getHealth());

	// display player 1 money
	fontScore.setFontColor(::PLAYER1_COLOR);
	_snprintf_s(buffer, ::BUF_SIZE, "$: %d", (int)player1Money);
    fontScore.print(buffer,::MONEY_X,::MONEY_Y);

	// display player 2 money
	fontScore.setFontColor(::PLAYER2_COLOR);
	_snprintf_s(buffer, ::BUF_SIZE, "$: %d", (int)player2Money);
    fontScore.print(buffer,::MONEY_X2,::MONEY_Y2);

    // draw player
    player.draw();
	player2.draw();
	guard2.draw();

	player1HealthBar.draw(tileCollisionNS::PLAYER1_COLOR);
	player2HealthBar.draw(tileCollisionNS::PLAYER2_COLOR);
	messageDialog->draw();

    if(menuOn)
        menu.draw();

    graphics->spriteEnd();
}

//=============================================================================
// The graphics device was lost.
// Release all reserved video memory so graphics device may be reset.
//=============================================================================
void TileCollision::releaseAll()
{
    menuTexture.onLostDevice();
    tileTextures.onLostDevice();
	tileTexturesPickups.onLostDevice();
	tileTexturesOverLay.onLostDevice();
    playerTexture.onLostDevice();
	messageDialog->onLostDevice();
	healthBarTexture.onLostDevice();

    Game::releaseAll();
    return;
}
//=============================================================================
// The grahics device has been reset.
// Recreate all surfaces.
//=============================================================================
void TileCollision::resetAll()
{
    playerTexture.onResetDevice();
    tileTextures.onResetDevice();
	tileTexturesPickups.onResetDevice();
	tileTexturesOverLay.onResetDevice();
    menuTexture.onResetDevice();
	messageDialog->onResetDevice();
	healthBarTexture.onResetDevice();
    
    Game::resetAll();
    return;
}
