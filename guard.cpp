// Programming 2D Games
// Copyright (c) 2011,2013 by: 
// Charles Kelly
// guard.cpp v1.0

#include "guard.h"

//=============================================================================
// default constructor
//=============================================================================
Guard::Guard() : Entity()
{
    spriteData.width = guardNS::WIDTH;   // guard size
    spriteData.height = guardNS::HEIGHT;
    spriteData.x = guardNS::X;           // location on screen
    spriteData.y = guardNS::Y;
    spriteData.rect.bottom = guardNS::HEIGHT;    // rectangle to select parts of an image
    spriteData.rect.right = guardNS::WIDTH;
    edge.top    = guardNS::EDGE_TOP;             // BOX collision edges
    edge.bottom = guardNS::EDGE_BOTTOM;
    edge.left   = guardNS::EDGE_LEFT;
    edge.right  = guardNS::EDGE_RIGHT;
    velocity.x = 0;
    velocity.y = 0;
    frameDelay = guardNS::ANIMATION_DELAY;
    startFrame = guardNS::START_FRAME;           // first frame of animation
    endFrame     = guardNS::END_FRAME;           // last frame of animation
    currentFrame = startFrame;
    radius = guardNS::WIDTH/2.0;
    collisionType = entityNS::ROTATED_BOX;
    mass = guardNS::MASS;
	turnAround = false;
	allowMovement = true;
	movement = 0.4f;
	directionModifier = 1;
}

//=============================================================================
// Initialize the guard.
// Post: returns true if successful, false if failed
//=============================================================================
bool Guard::initialize(Game *gamePtr, int width, int height, int ncols,
                            TextureManager *textureM)
{
    return(Entity::initialize(gamePtr, width, height, ncols, textureM));
}

//=============================================================================
// update
// typically called once per frame
// frameTime is used to regulate the speed of movement and animation
//=============================================================================
void Guard::update(float frameTime)
{
	Entity::update(frameTime);

	if(allowMovement)
	{
		movement = 0.4f;
	}
	else
		movement = 0;

	//spriteData.x += movement * directionModifier;
}
