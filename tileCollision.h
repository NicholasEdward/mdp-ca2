// Programming 2D Games
// Copyright (c) 2011,2013 by: 
// Charles Kelly
// tileCollision.h v1.0

#ifndef _TILECOLLISION_H        // Prevent multiple definitions if this 
#define _TILECOLLISION_H        // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

class TileCollision;

#include <string>
#include <fstream>
#include "game.h"
#include "textureManager.h"
#include "image.h"
#include "entity.h"
#include "player.h"
#include "guard.h"
#include "dashboard.h"
#include <time.h>

namespace tileCollisionNS
{
    const char FONT[] = "Arial Bold";  // font
	const COLOR_ARGB FONT_COLOR = graphicsNS::YELLOW;
    const int TILE_SIZE = 32;
    const int MAP_HEIGHT = 36;
    const int MAP_WIDTH = 36;
    const float SCROLL_RATE = 20;
    const float MAX_PLAYER_SPEED = 20;
    const USHORT EMPTY = 0;
    const USHORT MAX_COLLISION_TILE = 25;
    const int TILE_EDGE_TOP = -TILE_SIZE/2;  // for tile collision
    const int TILE_EDGE_BOTTOM = TILE_SIZE/2;
    const int TILE_EDGE_LEFT = -TILE_SIZE/2;
    const int TILE_EDGE_RIGHT = TILE_SIZE/2;
    const RECT TILE_EDGE = {TILE_EDGE_LEFT, TILE_EDGE_TOP, TILE_EDGE_RIGHT, TILE_EDGE_BOTTOM};
    const float TILE_MASS = 1.0f;
	const int HEALTHBAR_Y = 10;
	const int PLAYER1_HEALTHBAR_X = 10;
	const int PLAYER2_HEALTHBAR_X = GAME_WIDTH - 70;
	const int MAX_GUARDS = 10;         // max guards
	const float GUARD_LIMIT_LEFT = GAME_WIDTH/8;
	const float GUARD_LIMIT_RIGHT = GAME_WIDTH - GAME_WIDTH/8 - guardNS::WIDTH;
    const float GUARD_LIMIT_TOP = GAME_HEIGHT/8;
	const float GUARD_LIMIT_BOTTOM = GAME_HEIGHT - GAME_HEIGHT/8 - guardNS::HEIGHT;
	const COLOR_ARGB PLAYER1_COLOR = graphicsNS::YELLOW;
	const COLOR_ARGB PLAYER2_COLOR = graphicsNS::RED;
	const int FONT_SCORE_SIZE = 24;
	const int BUF_SIZE = 20;
	const int MONEY_Y = 30;
    const int MONEY_X = 10;
	const int MONEY_Y2 = 30;
    const int MONEY_X2 = GAME_WIDTH - 50;
}

// tileCollision is the class we create, it inherits from the Game class
class TileCollision : public Game
{
private:
    // game items
    TextureManager menuTexture;

    TextureManager tileTextures;        // tile textures
    TextureManager tileTexturesPickups;        // tile textures
    TextureManager playerTexture, player2Texture;
    TextureManager tileTexturesOverLay;        // tile textures
	TextureManager guardTexture;
	TextureManager healthBarTexture;
	Image tile2;
    Image   tile;
	Image   overLay;	
	Image   pickUp;
    Image   menu;
    Entity  tileEntity;            // for tile collision
	Entity  overLayEntity;               // pickup collision
	Entity  pickUpEntity;               // pickup collision
    Player    player, player2;
	Bar     player1HealthBar, player2HealthBar;          // health bar for player
	Guard guard2;
    std::ifstream mapIn;                // used to read map
	std::ifstream pickUpIn;
	std::ifstream overLayIn;

    float   mapX, mapY;
    bool    menuOn;
	bool questionAsked;
	bool questionAnswered, goodAns, badAns;
	int randomIndex, randomMoney, randomDamage;
	TextDX  fontScore;
	char buffer[tileCollisionNS::BUF_SIZE];
    // Tile numbers
    // 0 empty, 1-17 land, 50 water

    USHORT tileMap[tileCollisionNS::MAP_HEIGHT][tileCollisionNS::MAP_WIDTH];
	USHORT pickUpMap[tileCollisionNS::MAP_HEIGHT][tileCollisionNS::MAP_WIDTH];
	int player1Money, player2Money;
	int temp;
	USHORT overLayMap[tileCollisionNS::MAP_HEIGHT][tileCollisionNS::MAP_WIDTH];
	float questionTimer;

public:
	// Constructor
    TileCollision();
    // Destructor
    virtual ~TileCollision();
    // Initialize the game
    void initialize(HWND hwnd);
    void update();      // must override pure virtual from Game
    void ai();          // "

    void collisionTiles();  // "
	void collisionPickUps();
	void collisionOverLays();

	void pickupInteraction(int row, int col);

    void render();      // "
	void askQuestion(const char theQuestion[]);
	void posResponse();
	void negResponse();
	Player getPlayer2();
    void releaseAll();
    void resetAll();
};

#endif
