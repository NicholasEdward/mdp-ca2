// Programming 2D Games
// Copyright (c) 2011, 2013 by: 
// Charles Kelly
// player.h v1.0

#ifndef _PLAYER_H                 // Prevent multiple definitions if this 
#define _PLAYER_H                 // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

class Player;

#include "entity.h"
#include "constants.h"

namespace playerNS
{
    const int   WIDTH = 30;                 // image width (each frame)
    const int   HEIGHT = 30;                // image height
	const int   WIDTH2 = 31;                 // image width (each frame)
    const int   HEIGHT2 = 31;                // image height
    const int   X = GAME_WIDTH/2 - WIDTH/2; // location on screen
    const int   Y = GAME_HEIGHT/6 - HEIGHT;
    const int   EDGE_TOP = -5;             // For BOX and ROTATE_BOX collison.
    const int   EDGE_BOTTOM = 5;           // "   relative to center
    const int   EDGE_LEFT = -14;            // "
    const int   EDGE_RIGHT = 14;            // "
    const float SPEED = 200;                // pixels per second
    const float MAX_SPEED = 1000;
    const float ROTATION_RATE = 3.14f;      // radians per second
    const float MASS = 3.0f;                // mass
    const int   TEXTURE_COLS = 4;			// texture has 0 columns
    const int   START_FRAME = 0;            // animation starting frame
    const int   END_FRAME = 15;              // animation ending frame
    const float ANIMATION_DELAY = 0.1f;     // time between frames
}

// inherits from Entity class
class Player : public Entity
{
public:
    // constructor
    Player();

	bool allowMovement;
	COLOR_ARGB playerColour;

    // inherited member functions
    virtual bool initialize(Game *gamePtr, int width, int height, int ncols,
                            TextureManager *textureM);
    void update(float frameTime);
    void repair();
	void loseHealth(int amount);

    // new functions
    void setVelocityX(float v)  {velocity.x = v;}
    void setVelocityY(float v)  {velocity.y = v;}
};
#endif

